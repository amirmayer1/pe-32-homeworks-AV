// Экранирование - это способ записать в коде какие-либо символы, которые повторяют спец.символы самого JS.
// Например, для записи внутри кавычек - кавычки, которые необходимы для отображения пользователю, то перед ними используем - \
// let valueFirstName = prompt('Enter your first name');
// let valueLastName = prompt('Enter your last name');
// let valueBirthday = prompt('Введите дату в формате ДД.ММ.ГГГГ');
// let arrD = valueBirthday.split(".");
// do {
//     valueBirthday = prompt('Введите дату в формате ДД.ММ.ГГГГ');

// }
// while (arrD.forEach((item) => {
//         typeof Number(item) !== "number";

//     }));

// function createNewUser(firstName, lastName, birthday) {
//     return {
//         firstName,
//         lastName,
//         birthday,
//         getLogin() {
//             let login = firstName[0].toLowerCase() + lastName.toLowerCase();
//             return login
//         },
//         getAge() {
//             let now = new Date();
//             let today = new Date(now.getFullYear(), now.getMonth(), now.getDate());
//             let dob = new Date(birthday);

//             let dobnow = new Date(today.getFullYear(), dob.getMonth(), dob.getDate());
//             let age = today.getFullYear() - dob.getFullYear();

//             if (today < dobnow) {
//                 age = age - 1;
//             }
//             return age

//         },
//         getPassword() {
//             let password = firstName[0].toUpperCase() + lastName.toLowerCase() + birthday.substr(-4);
//             return password
//         }
//     }
// }
// const newUser = createNewUser(
//     firstName = valueFirstName,
//     lastName = valueLastName,
//     birthday = valueBirthday
// )

// console.log(newUser.getAge());
// console.log(newUser.getPassword());

let valueFirstName = prompt('Enter your first name');
let valueLastName = prompt('Enter your last name');
let valueBirthday = prompt('Введите дату в формате ДД.ММ.ГГГГ');

function createNewUser(firstName, lastName, birthday) {
    return {
        firstName,
        lastName,
        birthday,
        getLogin() {
            let login = firstName[0].toLowerCase() + lastName.toLowerCase();
            return login
        },
        getAge() {
            let ageArray = valueBirthday.split('.')
            let newAgeArray = new Date(Number(ageArray[2]), Number(ageArray[1]), Number(ageArray[0]))
            let todayDate = new Date()
            let todayDateNew = new Date(todayDate.getFullYear(), todayDate.getMonth(), todayDate.getDate())
            var nowYearDay = new Date(todayDateNew.getFullYear(), newAgeArray.getMonth(), newAgeArray.getDate())
            let age
            age = todayDateNew.getFullYear() - newAgeArray.getFullYear()
            if (todayDateNew < nowYearDay) {
                age = age - 1;
            }
            return age

        },
        getPassword() {
            let password = firstName[0].toUpperCase() + lastName.toLowerCase() + birthday.substr(-4);
            return password
        }
    }
}

const newUser = createNewUser(
    firstName = valueFirstName,
    lastName = valueLastName,
    birthday = valueBirthday
)

console.log(newUser.getAge());
console.log(newUser.getPassword());