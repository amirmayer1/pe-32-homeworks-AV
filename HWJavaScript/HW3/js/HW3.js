// 1.Функции нужны для того, чтобы не писать одно и тоже действие в повторяющихся частях программы. Функции являются основными частями программы
// 2.Аргументы функии мы можем передавать для того, чтобы функция производила над ними каки-либо операции(в зависимости от того, для чего эта функция содана)
//  и затем выводила результаты этих операций, которые можно будет также передавать другим функциям

let num1 = prompt("Enter number1");
while (isNaN(num1) || num1 === "") {
    num1 = prompt("Wrong data! Enter number1 again!");
}

let num2 = prompt("Enter number2");
while (isNaN(num2) || num2 === "") {
    num2 = prompt("Wrong data! Enter number2 again!");
}
let x = prompt("Enter one from this operations: +, -, *, /");
while (x !== "+" && x !== "-" && x !== "*" && x !== "/") {
    x = prompt("enter again, 1 from this operations: +, -, *, /");
}

function MatOperationsCalc(num1, num2, x) {
    num1 = +(num1)
    num2 = +(num2)
    switch (x) {
        case "+":
            console.log(num1 + num2);
            break;
        case "-":
            console.log(num1 - num2);
            break;
        case "*":
            console.log(num1 * num2);
            break;
        case "/":
            console.log(num1 / num2);
            break;
        default:
            alert("Wrong data!Reload the page");
    }
}
MatOperationsCalc(num1, num2, x)