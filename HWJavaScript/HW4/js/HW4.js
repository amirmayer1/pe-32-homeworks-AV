// Метод объекта - это функция, которая находится внутри объкта, 
// хранится внутри одного из его свойств и взаимодействует с остальными его свойствами

function createNewUser(firstName, lastName) {
    return {
        firstName,
        lastName,
        getLogin() {
            let login = firstName[0].toLowerCase() + lastName.toLowerCase();
            return login
        },
    }
}

const newUser = createNewUser(
    prompt('Enter your first name'),
    prompt('Enter your last name'),
)
console.log(newUser.getLogin());

// 2 вариант 

// function createNewUser(firstName, lastName) {

//     this.firstName = prompt('Enter your first name: ', '');
//     while (this.firstName === '') {
//         this.firstName = prompt('Enter your first name correct : ', '');
//     }

//     this.lastName = prompt('Enter your last name', '');
//     while (this.lastName === '') {
//         this.lastName = prompt('Enter your last name correct: ', '');
//     }

//     this.getLogin = function() {
//         let login = this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase();
//         return login;
//     }
// }


// let newUser = new createNewUser();
// console.log(newUser.getLogin());