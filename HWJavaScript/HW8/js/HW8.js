// Обработчик событий - это функция, которая сработает, как только событие произошло.Есть три способа назначения обработчиков событий:
// 1)Атрибут HTML: onclick="...".2)DOM-свойство: elem.onclick = function.
// 3)Специальные методы: elem.addEventListener для добавления, removeEventListener для удаления.

// Создать поле для ввода цены с валидацией. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).
// Технические требования:
// При загрузке страницы показать пользователю поле ввода (input) с надписью Price. Это поле будет служить для ввода числовых значений
// Поведение поля должно быть следующим:
// При фокусе на поле ввода - у него должна появиться рамка зеленого цвета. При потере фокуса она пропадает.
// Когда убран фокус с поля - его значение считывается, над полем создается span, в котором должен быть выведен текст: Текущая цена: ${значение из поля ввода}. Рядом с ним должна быть кнопка с крестиком (X). Значение внутри поля ввода окрашивается в зеленый цвет.
// При нажатии на Х - span с текстом и кнопка X должны быть удалены. Значение, введенное в поле ввода, обнуляется.
// Если пользователь ввел число меньше 0 - при потере фокуса подсвечивать поле ввода красной рамкой, под полем выводить фразу - Please enter correct price. span со значением при этом не создается.
// В папке img лежат примеры реализации поля ввода и создающегося span.

const div = document.getElementById("root");
const divContainer = document.createElement("div")
div.append(divContainer)
let text = document.createElement('span');
text.innerText = "Price: ";

let input = document.createElement('input');
input.placeholder = 'enter price';
input.addEventListener("focus", function() {
    input.classList.add("focused")
});
input.addEventListener("blur", function() {
    const span = document.createElement("span");
    const btn = document.createElement("button");

    btn.addEventListener("click", () => removespan(span));

    btn.innerHTML = "x"


    input.classList.remove("focused")
    if (input.value === "") {
        input.classList.remove("focused")
    } else if (Math.sign(input.value) === -1) {
        input.classList.remove("focused");
        input.classList.add("error");
        const p = document.createElement("p");
        p.classList.add("notice");
        p.innerText = "please inner correct price";
        input.after(p)
    } else {
        span.remove();
        span.innerText = `Текущая цена: ${input.value}`
        div.insertBefore(span, divContainer)
        span.appendChild(btn);
        input.classList.remove("error");
        input.classList.add("focused");
        let pNew = document.getElementsByClassName(".notice");
        pNew.remove()
    }

})

function removespan(span) {
    span.remove();
    input.value = "";
}

divContainer.prepend(text, input);