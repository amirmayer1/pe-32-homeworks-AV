// Циклы позволяют делать многократное выполнение повторяющихся вычислений. 
// Они помогают при написании кода, выполняя одну и ту же инструкцию, 
// образующих тело цикла, заданное число раз, либо пока заданное условие верно. 
// Однократное выполнение цикла называется итерацией.

function HWpart1() {
    let n = prompt("Enter number")
    while (isNaN(n)) {
        n = prompt("Only number can be accepted, enter again")
    }
    for (let i = 0; i <= n; i++) {
        if (i % 5 === 0 && i !== 0) {
            console.log(i)
        } else if (n < 5) {
            console.log("Sorry, no numbers")
            break
        }
    }
    for (let i = 0; i >= n; i--) {
        if (i % 5 === 0 && i !== 0) {
            console.log(i)
        } else if (n > -5) {
            console.log("Sorry, no numbers")
            break
        }
    }
}
HWpart1()

// function HWpart2() {
//     let n = prompt("Enter positive number1, it must be bigger than number 2")
//     while (isNaN(n) || n < 0) {
//         n = prompt("Only positive number can be accepted, enter again")
//     }
//     let m = prompt("Enter positive number2, it must be smaller than number 1")
//     while (isNaN(m) || m < 0) {
//         m = prompt("Only positive number can be accepted, enter again")
//     }
//     while (m >= n) {
//         m = prompt("Enter number2, it must be smaller than number 1")
//     }
//     CalcSimple:
//         for (let i = 2; i <= n; i++) {
//             for (let k = 2; k < i; k++) {
//                 if (i % k == 0) {
//                     continue CalcSimple
//                 }
//             }
//             if (i >= m) {
//                 console.log(i)
//             }
//         }
// }
// HWpart2()