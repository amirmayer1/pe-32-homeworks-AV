// Document Object Model (DOM) - это представление HTML документа в виде дерева с тегами, каждый HTML-тег является объектом.Теги становятся узлами-элементами и формируют структуру документа.
// Текст становится текстовыми узлами.Всё, что записано в HTML, есть и в DOM-дереве, даже комментарии. 

// Реализовать функцию, которая будет получать массив элементов и выводить их на страницу в виде списка. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонних библиотек (типа Jquery).
// Технические требования:
// Создать функцию, которая будет принимать на вход массив и опциональный второй аргумент parent - DOM-элемент, к которому будет прикреплен список (по дефолту должен быть document.body).
// Каждый из элементов массива вывести на страницу в виде пункта списка;
// Используйте шаблонные строки и метод map массива для формирования контента списка перед выведением его на страницу;
// Примеры массивов, которые можно выводить на экран:
// ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];


let list = document.createElement('ul');
list.innerText = "List:";
list.classList.add("Main-list");
document.body.prepend(list);

let string = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
let newString = string.map(function(element, parentElem = document.body) {
    return parentElem + element;
});
for (let i = 0; i < 6; i++) {
    let li = document.createElement('li');
    li.append(document.createTextNode(newString[i]));
    list.append(li);
}
setTimeout(() => list.remove(), 3000);