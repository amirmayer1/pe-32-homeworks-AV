// forEach - это метод массива, который используется для перебора массива. Он для каждого элемента массива вызывает функцию callback. 
// Этой функции он передаёт три параметра - очередной элемент массива, его номер и  массив, который перебирается
const arr = ['hello', 'world', 23, '23', null];

function filterBy(massive, type) {
    const newArray = massive.filter((item) => {
        return typeof item !== type;
    })

    return newArray;
}

console.log(filterBy(arr, 'string'));
console.log(filterBy(arr, 'number'));
console.log(filterBy(arr, 'object'));